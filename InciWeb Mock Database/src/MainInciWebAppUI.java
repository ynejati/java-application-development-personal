import com.sun.rowset.CachedRowSetImpl;

import javax.sql.RowSetEvent;
import javax.sql.RowSetListener;
import javax.sql.rowset.CachedRowSet;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.*;

// TODO Update GO button action listener
// TODO Implement CachedRowSet listeners
// TODO Add input restrictions on Acres(double) and Updated (YYYY-MM-DD) text fields

// TODO Other....
// TODO Clean up comments and code
// TODO Preform tests
// TODO Publish applet


/**
 * File: MainInciWebAppUI.java
 * Author: Yousuf Nejati
 * Date: October 13th 2015
 * Purpose: Develop a UI that preforms basic database operations on mock national wildfire data
 * as taken from the National Wildlands Coordinating Group's Incident Information System called
 * InciWeb. The hope is to use this as a launching point in order to develop technological tools
 * for natural resource related activities.
 * Technologies: MySQL
 */
public class MainInciWebAppUI extends JFrame implements RowSetListener, ActionListener {


    // Instance variables of the MainInciWebAppUI
    private JRadioButton newEntry;
    private JRadioButton deleteEntry;
    JRadioButton updateEntry;
    ButtonGroup group;
    private JTable table;
    private final String username = "user";
    private final String password = "p@ssword";
    private Connection connection;
    private InciTableModel inciTableModel;

    /**
     * Class constructor builds the UI and table, establishes a connection to the
     * database and initializes the CachedRowSet. Takes in a JDBCUtilities object
     * that handles JDBC and SQL connection, applying our desired settings
     */
    public MainInciWebAppUI() {

        // Sets the tile of the parent JFrame
        super("Incident Information System Database");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Set window attributes
        setResizable(false);
        setMinimumSize(new Dimension(1000, 400));

        // Create two new panels
        JPanel bottomPanel = new JPanel();
        JPanel topPanel = new JPanel();

        addComponentsToBottomPane(bottomPanel);
        addComponentsToTopPane(topPanel);

        // Add the panels, both left and right, to the parent JFrame
        getContentPane().add(bottomPanel, BorderLayout.PAGE_END);
        getContentPane().add(topPanel, BorderLayout.CENTER);

        // Add go button action listener
        goButton.addActionListener(this);

        // Register the MySQL Connector/J.
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        /**
         * Close the connection when the window closes or exit application
         * by using a WindowListener that implements the windowClosing method
         * of the WindowAdapter class
         */
        addWindowListener(new WindowAdapter() {
            @Override public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                try {
                    connection.close();
                } catch (SQLException closeEx) {
                    JOptionPane.showMessageDialog(null, "Error closing connection");
                    System.out.println(closeEx.toString());
                    System.exit(0);
                }
            }
        });

        // Pack frame components
        pack();
    }

    /**
     * Adds the components to the bottom panel, called by the class constructor
     * to build the UI. Components include radio buttons and the execution button
     * or "Go"
     *
     * @param bottomPane or bottom panel of the parent frame
     */
    private void addComponentsToBottomPane(Container bottomPane) {

        // Initializes a bunch of global radio buttons
        // Selecting this radio will display new entry popup frame upon user commit
        newEntry = new JRadioButton("New record");

        // Pulls from the row selected in the JTable upon user commit, prompts user with confirm
        // prior to finalizing record delete
        deleteEntry = new JRadioButton("Delete record");

        // Pulls data from the selected row in the JTable upon user commit, populates popup frame
        updateEntry = new JRadioButton("Update record");

        // Initializes a global button group
        group = new ButtonGroup();

        // Sets the new entry button to default selected
        newEntry.setSelected(true);

        // Adds these radio buttons to the button group so that are mutually exclusive
        group.add(newEntry);
        group.add(deleteEntry);
        group.add(updateEntry);

        // Define the layout of this panel
        bottomPane.setLayout(new GridBagLayout());

        // ROW 1: Adds everything on row three of the panel, radio buttons and "Go" button
        GridBagConstraints cTaskSlect = new GridBagConstraints();
        cTaskSlect.gridx = 0;
        cTaskSlect.gridy = 0;
        cTaskSlect.ipadx = 15;
        bottomPane.add(new JLabel("Select task: ", SwingConstants.LEFT), cTaskSlect);

        GridBagConstraints cNewEnt = new GridBagConstraints();
        cNewEnt.gridx = 1;
        cNewEnt.gridy = 0;
        cNewEnt.ipadx = 15;
        bottomPane.add(newEntry, cNewEnt);

        GridBagConstraints cDelEnt = new GridBagConstraints();
        cDelEnt.gridx = 2;
        cDelEnt.gridy = 0;
        cDelEnt.ipadx = 15;
        bottomPane.add(deleteEntry, cDelEnt);

        GridBagConstraints cUpEnt = new GridBagConstraints();
        cUpEnt.gridx = 3;
        cUpEnt.gridy = 0;
        cUpEnt.ipadx = 15;
        bottomPane.add(updateEntry, cUpEnt);

        // ROW 2: Add the "GO" button and stretch it among two cells
        GridBagConstraints cGoBttn = new GridBagConstraints();
        cGoBttn.fill = GridBagConstraints.HORIZONTAL;
        cGoBttn.gridx = 1;
        cGoBttn.gridy = 1;
        cGoBttn.gridwidth = 2;
        cGoBttn.ipady = 4;
        cGoBttn.weighty = 1.0;
        bottomPane.add(goButton, cGoBttn);
    }

    /**
     * This method adds components to the bottom panel, called by the class constructor
     * to build to UI. In this case, adds the JTable populated with the cachedRowSet
     * pulled from local SQL database
     *
     * @param topPane or top panel of the parent frame
     */
    private void addComponentsToTopPane(Container topPane) {

        // This block retrieves contents of database table and store its contents in
        // CachedRowSet object and initialize the JTable Swing components upon
        // window construction
        try {
            // Create new CachedRowSet object
            // Start downloading and populating UI JTable from
            // table designated in database defined
            CachedRowSet cachedRowSet = getContentsOfTable();
            inciTableModel = new InciTableModel(cachedRowSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        topPane.setLayout(new GridBagLayout());

        inciTableModel.addEventHandlersToRowSet(MainInciWebAppUI.this);
        table = new JTable();
        // Build table in UI to accommodate data from data source
        table.setModel(inciTableModel);
        table.setFillsViewportHeight(true);
        table.setPreferredScrollableViewportSize(new Dimension(1000, 400));
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 10;
        c.gridwidth = 20;
        table.doLayout();
        topPane.add(new JScrollPane(table), c);
    }

    /**
     * "GO" button TODO
     * Gathers input from comboBoxes, textfields, and radio's to determine action
     * <p>
     * Currently, it does not do anything except for prompting the user with a message
     * The full implementation of this seems beyond what is required for this discussion
     * assignment
     */
    JButton goButton = new JButton(new AbstractAction("GO") {
        @Override public void actionPerformed(ActionEvent e) {

            if (newEntry.isSelected()) {
                // pull up the new entry dialog
                // TODO
            } else if (updateEntry.isSelected()) {
                // check to see if a row is selected
                // if no, prompt user
                // if yes, continue

                // pull up the update entry dialog based up on the selection
                // TODO
            } else { // delete must be selected

                // check to see if a row is selected
                // if no, prompt user
                // if yes, continue

                // pop up dialog confirmation
                // if yes, remove row
                // if no, cancel operations
                // TODO
            }
        }
    });

    /**
     * Uses DriverManager (for simplicity) to establish a connection to our database
     * SSL is set to false and autoCommit is relaxed
     *
     * @return connection
     */
    public Connection getConnection() throws SQLException {

        String databaseURL =
            "jdbc:mysql://localhost:3306/MySQL" + "?relaxAutoCommit=true" + "&useSSL=false";
        Connection conn = DriverManager.getConnection(databaseURL, username, password);
        return conn;
    }

    /**
     * Gets the contents for the JTable as a cached row set by executing a simple SQL command
     * selecting columns and rows of our databse
     *
     * @return rowSet
     * @throws SQLException
     */
    public CachedRowSet getContentsOfTable() throws SQLException {

        CachedRowSet crs = null;

        try {

            connection = getConnection();

            crs = new CachedRowSetImpl();
            //
            crs.setType(ResultSet.TYPE_SCROLL_INSENSITIVE);
            //
            crs.setConcurrency(ResultSet.CONCUR_UPDATABLE);
            // Set username of CachedRowSet
            crs.setUsername(username);
            crs.setPassword(password);
            // Set key columns to uniquely identify a row set from the database table (ID)
            // Needed in order to make updates to the CRS, and save to the database
            final int[] keys = {9};
            crs.setKeyColumns(keys);
            crs.setCommand("SELECT * FROM incident_info.Incident_Master");

            crs.execute(connection);

        } catch (SQLException e) {
            System.out.println("SQL error while getContentsOfTable() " + e.toString());
        }

        return crs;
    }

    /**
     * @throws SQLException
     */
    private void createNewTableModel() throws SQLException {

        inciTableModel = new InciTableModel(getContentsOfTable());
        inciTableModel.addEventHandlersToRowSet(this);
        table.setModel(inciTableModel);
    }

    /**
     * Update data source
     */
    public void updateDataSource() {
        //        try {
        //            // Call acceptChanges() to save changes to data source
        //            cachedRowSet.acceptChanges();
        //        } catch (SyncProviderException e) {
        //            e.printStackTrace();
        //        }
    }

    @Override public void rowSetChanged(RowSetEvent event) {
        // TODO
    }

    @Override public void rowChanged(RowSetEvent event) {
        // TODO
    }

    @Override public void cursorMoved(RowSetEvent event) {
        // TODO
    }

    @Override public void actionPerformed(ActionEvent e) {

    }

    /**
     * Driver, PSVM, creates a new gui objects and sets it to visible
     *
     * @param args
     */
    public static void main(String[] args) {
        MainInciWebAppUI gui = new MainInciWebAppUI();
        gui.setVisible(true);
    }
}
