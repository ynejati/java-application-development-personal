import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * File: BaseUpdateInciWebAppUI.java
 * Author: Yousuf Nejati
 * Date: January 17th 2015
 * Purpose: Used with MainInciWebUI class. New object is created when either the user
 * has selected to create a new record or update a current record.
 */
public class BaseUpdateInciWebAppUI extends JFrame implements ActionListener {

    // Declares and initializes arrays used for our JComboBoxes. These are used to preform
    // operations on our database such as adding, updating, and removing rows from our RowSet
    private final String[] statesList =
        {"Select State", "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID",
            "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT",
            "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
            "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};
    // Provides user with incident status options
    private final String[] statusList = {"Select Status", "Active", "Inactive"};
    // Provides incident type options
    private final String[] typeList =
        {"Select Type", "Prescribed Fire", "Wildfire", "Burned Area Emergency Response"};
    // Provides user with incident containment options in varying approximated increments
    private final String[] percentContList =
        {"Select Percent Contained", "0%", "1%", "5%", "10%", "15%", "20%", "25%", "30%", "35%",
            "40%", "45%", "50%", "55%", "60%", "65%", "70%", "75%", "80%", "85%", "90%", "95%",
            "100%"};

    // ID of the row to update or add
    private static int rowID;
    private static String action;

    /**
     * Class Constructor
     *
     * @param title  of the frame
     * @param rowID  id of new row or row to update
     * @param action used to set name of action button
     */
    public BaseUpdateInciWebAppUI(String title, int rowID, String action) {
        super(title);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setRowID(rowID);
        setAction(action);
        addComponents(this);
        this.setVisible(true);
    }

    public static int getRowID() {
        return rowID;
    }

    public static void setRowID(int rowId) {
        BaseUpdateInciWebAppUI.rowID = rowId;
    }

    public static String getAction() {
        return action;
    }

    public static void setAction(String action) {
        BaseUpdateInciWebAppUI.action = action;
    }

    /**
     * Action button listener, checks, confirms and updates row
     */
    JButton actionBttn = new JButton(new AbstractAction(action) {
        @Override public void actionPerformed(ActionEvent e) {
            // Confirm all entries are updated and formatted properly
            // Update database row
            // TODO

        }
    });

    /**
     * Add the components to the frame
     *
     * @param parent
     */
    private void addComponents(Container parent) {

        // Declare various text fields
        JTextField incident, unit, acres, updated;

        // Declare various labels
        JLabel label_incidentT, label_typeT, label_unitT, label_stateT,
            label_statusT, label_percentContainedT, label_acresT, label_updatedT;

        // Declare various combo boxes
        JComboBox<String> stateBox, statusBox, typeBox, percentContainedBox;

        // Set layout of parent to grid layout with 2 columns and undetermined
        // number of rows
        parent.setLayout(new GridLayout(0, 2));

        // Initializes everything going in on ROW 1
        // User enters incident name here
        label_incidentT = new JLabel("Incident");
        label_typeT = new JLabel("Type");
        label_unitT = new JLabel("Unit");
        label_stateT = new JLabel("State");
        label_statusT = new JLabel("Status");
        label_percentContainedT = new JLabel("% contained");
        label_acresT = new JLabel("Acres");
        label_updatedT = new JLabel("Updated");

        // Row 1 Name
        parent.add(label_incidentT);
        incident = new JTextField();
        parent.add(incident);

        // Row 2 Type
        parent.add(label_typeT);
        typeBox = new JComboBox<>(typeList);
        parent.add(typeBox);

        // Row 3 Unit
        parent.add(label_unitT);
        unit = new JTextField();
        parent.add(unit);

        // Row 4 State
        parent.add(label_stateT);
        stateBox = new JComboBox<>(statesList);
        parent.add(stateBox);

        // Row 5 Status
        parent.add(label_statusT);
        statusBox = new JComboBox<>(statusList);
        parent.add(statusBox);

        // Row 6 PercentContained
        parent.add(label_percentContainedT);
        percentContainedBox = new JComboBox<>(percentContList);
        parent.add(percentContainedBox);

        // Row 7 Acres
        parent.add(label_acresT);
        acres = new JTextField();
        parent.add(acres);

        // Row 8 Updated
        parent.add(label_updatedT);
        updated = new JTextField();
        parent.add(updated);

        // Row 9 ActionButton
        parent.add(actionBttn);
    }


    @Override public void actionPerformed(ActionEvent e) {

    }
}
