import javax.sql.RowSetListener;
import javax.sql.rowset.CachedRowSet;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * File: InciTableModel.java
 * Author: Yousuf Nejati
 * Date: December 23rd 2015
 * Purpose: Implement table model to be used to build JTable in ApplicationInterface class.
 * Used the help of Java JDBC tutorial to build this class in order to understand why and how to
 * properly implement a TableModel used in conjunction with SQL database.
 */
public class InciTableModel implements TableModel {

    // Instance of CachedRowSet initialized in the class constructor
    private CachedRowSet inciRowSet;
    // Gets information about the types and properties of the columns in the ResultSet object
    // very useful in this regard
    private ResultSetMetaData metaData;
    // Hold rows and columns
    private int numOfColumns;
    private int numOfRows;

    /**
     * Class constructor
     * TODO UPDATE Comments
     *
     * @param cachedRowSet, takes CachedRowSet object
     * @throws SQLException
     */
    public InciTableModel(CachedRowSet cachedRowSet) throws SQLException {

        this.inciRowSet = cachedRowSet;
        this.metaData = this.inciRowSet.getMetaData();

        assert (metaData!=null);

        numOfColumns = metaData.getColumnCount();//TODO THROWS NULL POINTER EXCEPTION getting colmns

        // Retrieve the number of rows.
        this.inciRowSet.beforeFirst();
        this.numOfRows = 0;

        while (this.inciRowSet.next()) {
            this.numOfRows++;
        }
        this.inciRowSet.beforeFirst();
    }

    /**
     * Gets instance variable numOfRows
     *
     * @return numOfRows
     */
    @Override public int getRowCount() {
        return numOfRows;
    }

    /**
     * Gets instance variable numOfColumns
     *
     * @return numOfColumns
     */
    @Override public int getColumnCount() {
        return numOfColumns;
    }

    /**
     * Using the MetaData object, gets the column name
     *
     * @param columnIndex
     * @return columnName
     */
    @Override public String getColumnName(int columnIndex) {

        try {
            // TODO UPDATE Comments
            return this.metaData.getColumnName(columnIndex + 1);
        } catch (SQLException e) {
            return "SQL exception has occurred" + e.toString();
        }
    }

    /**
     * Method from interface TableModel; returns the most specific superclass for
     * all cell values in the specified column. To keep things simple, all data
     * in the table are converted to String objects; hence, this method returns
     * the String class. (SOURCE: Oracle)
     *
     * @param columnIndex
     * @return
     */
    @Override public Class getColumnClass(int columnIndex) {
        // TODO UPDATE Comments
        return String.class;
    }

    /**
     * Cells not editable, no implementation necessary
     *
     * @param rowIndex
     * @param columnIndex
     * @return false
     */
    @Override public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    /**
     * @param rowIndex
     * @param columnIndex
     * @return object
     */
    @Override public Object getValueAt(int rowIndex, int columnIndex) {

        try {

            this.inciRowSet.absolute(rowIndex + 1);
            Object o = null;
            o = this.inciRowSet.getObject(columnIndex + 1);
            if (o == null) {
                return null;
            } else {
                return o.toString();
            }
        } catch (SQLException e) {
            return "SQL exception has occurred" + e.toString();
        }
    }

    /**
     * Don't need
     *
     * @param aValue
     * @param rowIndex
     * @param columnIndex
     */
    @Override public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        System.out.println("Calling setValueAt " + " Row: " + rowIndex + " Column: " + columnIndex);
    }

    /**
     * Do not need
     *
     * @param l
     */
    @Override public void addTableModelListener(TableModelListener l) {

    }

    /**
     * Do not need
     *
     * @param l
     */
    @Override public void removeTableModelListener(TableModelListener l) {

    }

    /**
     * @param listener
     */
    public void addEventHandlersToRowSet(RowSetListener listener) {

        this.inciRowSet.addRowSetListener(listener);
    }

    /**
     * Collects input to insert new row into table, updating CachedRowSet
     *
     * @param incidentName
     * @param type
     * @param unit
     * @param state
     * @param status
     * @param percentContained
     * @param acresBurned
     * @param date
     * @param incidentID
     */
    public void insertRow(String incidentName, String type, String unit, String state,
        String status, double percentContained, int acresBurned, String date, int incidentID)
        throws SQLException {

        try {

            this.inciRowSet.moveToInsertRow();
            this.inciRowSet.updateString("NAME", incidentName);
            this.inciRowSet.updateString("TYPE", type);
            this.inciRowSet.updateString("UNIT", unit);
            this.inciRowSet.updateString("STATE", state);
            this.inciRowSet.updateString("STATUS", status);
            this.inciRowSet.updateDouble("PERCENT_CONTAINED", percentContained);
            this.inciRowSet.updateInt("ACRES", acresBurned);
            this.inciRowSet.updateString("UPDATED", date);
            this.inciRowSet.updateInt("ID", incidentID);

        } catch (SQLException e) {
            System.out.println("SQL Exception" + e);
        }
    }

    /**
     * Called when we'd like to release database and JDBC resources from ResultSet object
     */
    public void close() {
        try {
            // getStatement() inherited from ResultSet
            // Retrieves the statement object (used for executing a static SQL statement) from
            // the ResultSet object. Releases statement objects database and JDBC resources
            // immediately instead of waiting upon auto close. Good practice to release resources
            // when don
            inciRowSet.getStatement().close();
            // Throws SQLException if database error occurs
        } catch (SQLException e) {
            System.out.println("SQL exception" + e);
        }
    }

    /**
     * Auto close upon garbage collection
     */
    protected void finalize() {
        close();

    }

    /**
     * Gets instance of CachedRowSet
     *
     * @return inciRowSet
     */
    public CachedRowSet getInciRowSet() {
        return inciRowSet;
    }
}
